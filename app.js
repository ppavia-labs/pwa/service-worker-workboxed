const apiKey    = "1e086e084c2248b09f94f5220aba2f60";
const main      = document.querySelector('main');
const sources   = document.querySelector('#sourceSelector');
const defaultSource   = "bbc-news";

window.addEventListener('load', async e => {
    updateNews();
    await updateSources();
    sources.value  = defaultSource;

    sources.addEventListener('change', e => {
        updateNews(e.target.value);
    });

    if ( 'serviceWorker' in navigator ) {
        try {
            navigator.serviceWorker.register('sw.js');
            console.log("SW registrated");
        }
        catch (error) {
            console.log("registration failed");
        }
    }
});

async function updateSources () {
    const res   = await fetch(`https://newsapi.org/v2/sources?apiKey=${apiKey}`);
    const json  = await res.json();
    sources.innerHTML  = json.sources
    .map(src => `<option value="${src.id}">${src.name}</option>`)
    .join('\n');
}

async function updateNews (source = defaultSource) {
    const res   = await fetch (`https://newsapi.org/v2/everything?sources=${source}&q=bitcoin&apiKey=${apiKey}`);

    if ( res ) {
        const json  = await res.json();
        main.innerHTML  = json.articles.map(createArticle).join('\n');
    }

}

function createArticle (article) {
    const img   = article.urlToImage ? `<img width="300px" src="${article.urlToImage}"/>` : "";
    return `
        <div class="article">
            <a href="${article.url}">
                <h2>${article.title}</h2>
                <h3>${article.source.name}</h3>               
                ${img}
                <p>${article.description}</p>
            </a>
        </div>
    `;
}